let snakeArr = [
    [10,12],
    [10,13],
    [10,14],
]

let snakeSize = 3;

let signs = {
    '#':  'border',
    ' ': 'gamefield',
    '*': 'food',
    'S': 'snake'
}

let direction = 4;

const map = [
    '################################################################################',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '#                                                                              #',
    '################################################################################',
];

$( document ).ready(function() {
    //mapArr[Math.round(Math.random() * (78)+1)][Math.round(Math.random() * (78)+1)] = '*';
    var loop = setInterval(repaint, 100);
});


$(document).keypress(function(e) {
    //console.log(e.which)
    switch (e.which) {
        case 13:
            snakeSize++;
            break;
        case 119:
            direction = 1;
            break;
        case 115:
            direction = 2;
            break;
        case 97:
            direction = 3;
            break;
        case 100:
            direction = 4;
            break;
    }
});

function repaint() {


    switch (direction) {
        case 1:
            snakeArr.push([snakeArr[snakeArr.length - 1][0] - 1, snakeArr[snakeArr.length - 1][1]])
            break;
        case 2:
            snakeArr.push([snakeArr[snakeArr.length - 1][0] + 1, snakeArr[snakeArr.length - 1][1]])
            break;
        case 3:
            snakeArr.push([snakeArr[snakeArr.length - 1][0], snakeArr[snakeArr.length - 1][1] - 1])
            break;
        case 4:
            snakeArr.push([snakeArr[snakeArr.length - 1][0], snakeArr[snakeArr.length - 1][1] + 1])
            break;
    }

    if(snakeSize < snakeArr.length) {

       snakeArr.shift();

    }

    let mapArr = [];

    map.forEach(line => {
        let maprow = [];
        line.split('').forEach(sign => {
            maprow.push(sign);
        });
        mapArr.push(maprow);
    });

    snakeArr.forEach(field => {
        mapArr[field[0]][field[1]] = 'S'
    })



    $("section").remove();
    $("body " ).append(`<section></section>`)
    mapArr.forEach(line => {
        line.forEach(sign => {
            $("section"  ).append(`<div class='${signs[sign]}'></div>`
            )

        });

    });
}

